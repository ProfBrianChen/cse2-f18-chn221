////////////
// CSE 002 
// Lab05 
// Chloe Norvell
// 4 Oct 2018
//
// program asks user for information about a course and uses loops to ensure the user input is the correct type then 
// prints all of the user information to the screen

import java.util.Scanner; // import scanner class 

public class Lab05{
  public static void main(String[] args){
    int courseNumber, timesMetWeekly, courseTimeHour, courseTimeMinutes, numStudents; // initializing int variables
    String deptName, instructorName, invalidInput, nextInput; // int string variables
    
    Scanner myScanner = new Scanner (System.in); // creating instance that will take input from STDIN
    
    // asking for user inputs
    System.out.println("Enter course number: ");
    while (!myScanner.hasNextInt()){ // enters loop if input isn't an integer
      invalidInput = myScanner.next(); // removes input from 'conveyer belt'
      System.out.println("Invalid course number. Please enter an integer value: ");
    }
    courseNumber = myScanner.nextInt(); // stores valid input
    
    nextInput = myScanner.nextLine(); // reads next input as line
    
    System.out.println("Enter department name: "); 
    while (!myScanner.hasNextLine()){ // enters loop if input isn't an integer
      invalidInput = myScanner.next(); // removes input from 'conveyer belt'
      System.out.println("Invalid department name. Please enter a string: ");
    }
    deptName = myScanner.nextLine(); // stores valid input
  
    System.out.println("Enter the number of times a class meets in a week: ");
    while (!myScanner.hasNextInt()){ // enters loop if input isn't an integer
      invalidInput = myScanner.next(); // removes input from 'conveyer belt'
      System.out.println("Invalid number of times. Please enter an integer value: ");
    }
    timesMetWeekly = myScanner.nextInt(); // stores valid input
    
    
    System.out.println("Enter hour class starts: "); 
    while (!myScanner.hasNextInt()){ // enters loop if input isn't an integer
      invalidInput = myScanner.next(); // removes input from 'conveyer belt'
      System.out.println("Invalid time. Please enter an integer value: ");
    }
    courseTimeHour = myScanner.nextInt(); // stores valid inputs
    
    System.out.println("Enter minutes class starts:"); 
    while (!myScanner.hasNextInt()){ // enters loop if input isn't an integer
      invalidInput = myScanner.next(); // removes input from 'conveyer belt'
      System.out.println("Invalid time. Please enter a integer value: ");
    }
    courseTimeMinutes = myScanner.nextInt(); // stores valid inputs
    
    nextInput = myScanner.nextLine(); // reads next input as line
    
    System.out.println("Enter instructor's name: "); 
    while (!myScanner.hasNextLine()){ // enters loop if input isn't an integer
      invalidInput = myScanner.next(); // removes input from 'conveyer belt'
      System.out.println("Invalid entry. Please enter a string: ");
    }
    instructorName = myScanner.nextLine(); // stores valid inputs
    
    System.out.println("Enter the number of students in the class: "); 
    while (!myScanner.hasNextInt()){ // enters loop if input isn't an integer
      invalidInput = myScanner.next(); // removes input from 'conveyer belt'
      System.out.println("Invalid number. Please enter an integer: ");
    }
    
    numStudents = myScanner.nextInt(); // stores valid inputs
    
    // print outputs
    System.out.println("Course Number: " + courseNumber);
    System.out.println("Department Name: " + deptName);
    System.out.println("Number of Times Class Meets Weekly: " + timesMetWeekly);
    System.out.println("Time Class Meets: " + courseTimeHour + ":" + courseTimeMinutes);
    System.out.println("Instructor's Name: " + instructorName);
    System.out.println("Number of Students in Class: " + numStudents);
    
  }
}