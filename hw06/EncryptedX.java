// ////////////
// CSE 002 
// Hw05 
// Chloe Norvell
// 9 Oct 2018
// 
// 

import java.util.Scanner; // imports scanner class

public class EncryptedX{
   public static void main(String[] args){
     int sizeOfX;
     String invalidInput; 
     Scanner myScanner = new Scanner (System.in); // creating instance that will take input from STDIN

     System.out.println("Enter a size from 0-100: ");
     
     while (!myScanner.hasNextInt()){ // enters loop if input isn't an integer
       invalidInput = myScanner.next(); // removes input from 'conveyer belt'
     System.out.println("Invalid input. Please enter an integer 0-100:");
      }
     
     sizeOfX = myScanner.nextInt(); 
     
     if (sizeOfX < 0 || sizeOfX > 100){ // rejects inputs outside accepted range
       System.out.println("Invalid input. Please enter an integer 0-100:");
       sizeOfX = myScanner.nextInt(); 
     }
     
     for(int i = 0; i <= sizeOfX; i++){
       for (int j = 0; j <= sizeOfX; j++){
         if((j == i) || (j == (sizeOfX - i))){
           System.out.print(" ");
         } else {
           System.out.print("*");
         }
       }
       System.out.println("");
     }
     
   }
}