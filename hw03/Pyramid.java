////////////
// CSE 002 
// Hw03 Pyramid
// Chloe Norvell
// 18 Sept 2018
//
import java.util.Scanner; // import scanner class 

public class Pyramid{
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in); // creating instance that will take input from STDIN
    
    System.out.println("The square side of the pyramid is (input length): "); 
    // ask for the input for square side of pyramid
    double squareSideLength = myScanner.nextDouble(); 
    // accept user input for square side of pyramid
    
    System.out.println("The height of the pyramid is (input height): "); 
    // ask for height of pyramid
    double pyramidHeight = myScanner.nextDouble(); 
    // accept user input for height of pyramid
    
    double lengthTimesWidth = (squareSideLength * squareSideLength); // length times width
    double oneThird = (1.0 / 3.0);
    double pyramidVolume = (oneThird) * ((lengthTimesWidth) * (pyramidHeight)); 
    // one third length times width times height
    
    System.out.println("The volume of the pyramid is: " + pyramidVolume); // print answer in cubic miles
  }
}