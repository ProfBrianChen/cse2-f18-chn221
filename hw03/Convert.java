////////////
// CSE 002 
// Hw03 Convert
// Chloe Norvell
// 18 Sept 2018
//

import java.util.Scanner; // import scanner class 

public class Convert{
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in); // creating instance that will take input from STDIN
    
    System.out.println("Enter the number of acres of land affected by hurricane precipitation "); 
    // ask for the number of acres of land affected by hurricane precipitation
    double acresOfLand = myScanner.nextDouble(); 
    // accept user input for acres of land affected by hurricane precipitation
    
    System.out.println("Enter how many inches of rain were dropped on average "); 
    // ask for average inches of rain dropped
    double avgInchesRain = myScanner.nextDouble(); 
    // accept user input for average inches of rain dropped
    
    double acreInches = acresOfLand * avgInchesRain; // multiply to turn into acre-inches
    double acreFeet = acreInches * (1.0 / 12.0); // convert to acreFeet
    double powerOfTen = Math.pow(10, 6); // 10 to the power 6
    double conversionFactorToCubicMiles = (3.379 * powerOfTen); // get conversion factor to convert to cubic miles
    
    double cubicMiles = (acreFeet / conversionFactorToCubicMiles);
    
    System.out.println(cubicMiles + " cubic miles"); // print answer in cubic miles
  }
}
     
  
      
    