////////////
// CSE 002 Hello World
// Chloe Norvell

public class HelloWorld{
  
  public static void main(String args[]){
    // prints Hello, World to terminal window
    System.out.println("Hello, World");
  }
  
}
