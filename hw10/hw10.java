// ////////////
// CSE 002 
// HW10
// Chloe Norvell
// 4 Dec 2018
// 
import java.util.Scanner;
import java.util.Arrays;
public class hw10{
  public static void printBoard(String [][] array){
    for(int i = 0; i < array.length; i++){
        for (int j = 0; j < array[i].length; j++){
          System.out.print(array[i][j] + " ");
        }
        System.out.println();
    }
  }
  public static int getPosition(String [][] array){
    Scanner myScanner = new Scanner(System.in);
    int pos = 0;
    boolean check = false;
    while (!check){
      check = myScanner.hasNextInt();
      if (check){
        //System.out.println("Please enter your desired position:");
        pos = myScanner.nextInt();
        if (pos < 1 || pos > 9){
          System.out.println("Error: Invalid position. Please enter a position within range:");
          check = false;
          continue;
        }
        switch(pos){
          case 1: 
            if (array [0][0] == "O" || array[0][0] == "X"){
              System.out.println("Error: Invalid position. Please enter an unfilled positon:");
              check = false;
              continue;
            }
            break;
          case 2:
            if (array [0][1] == "O" || array[0][1] == "X"){
              System.out.println("Error: Invalid position. Please enter an unfilled positon:");
              check = false;
              continue;
            }
            break;
          case 3:
            if (array [0][2] == "O" || array[0][2] == "X"){
              System.out.println("Error: Invalid position. Please enter an unfilled positon:");
              check = false;
              continue;
            }
            break;
          case 4:
            if (array [1][0] == "O" || array[1][0] == "X"){
              System.out.println("Error: Invalid position. Please enter an unfilled positon:");
              check = false;
              continue;
            }
            break;
          case 5:
            if (array [1][1] == "O" || array[1][1] == "X"){
              System.out.println("Error: Invalid position. Please enter an unfilled positon:");
              check = false;
              continue;
            }
            break;
          case 6:
            if (array [1][2] == "O" || array[1][2] == "X"){
              System.out.println("Error: Invalid position. Please enter an unfilled positon:");
              check = false;
              continue;
            }
            break;
          case 7:
            if (array [2][0] == "O" || array[2][0] == "X"){
              System.out.println("Error: Invalid position. Please enter an unfilled positon:");
              check = false;
              continue;
            }
            break;
          case 8:
             if (array [2][1] == "O" || array[2][1] == "X"){
              System.out.println("Error: Invalid position. Please enter an unfilled positon:");
              check = false;
              continue;
            }
            break;
          case 9:
             if (array [2][2] == "O" || array[2][2] == "X"){
              System.out.println("Error: Invalid position. Please enter an unfilled positon:");
              check = false;
              continue;
            }
            break;
        }
        break;
      }
      System.out.println("Error: Please enter an integer:");
     myScanner.next();
    }
    return pos;
  }
  public static boolean checkForWin(String [][] array){
    if (array[0][0] == array[0][1] && array[0][0] == array[0][2]){
      System.out.println("The " + array[0][0] + " player has won");
      return true;
    } else if (array[1][0] == array[1][1] && array[1][0] == array[1][2]){
      System.out.println("The " + array[1][0] + " player has won");
      return true;
    } else if (array[2][0] == array[2][1] && array[2][0] == array[2][2]){
      System.out.println("The " + array[2][0] + " player has won");
      return true;
    } else if (array[0][0] == array[1][0] && array[0][0] == array[2][0]){
      System.out.println("The " + array[0][0] + " player has won");
      return true;
    }else if (array[0][1] == array[1][1] && array[0][1] == array[2][1]){
      System.out.println("The " + array[0][1] + " player has won");
      return true;
    } else if (array[0][2] == array[1][2] && array[0][2] == array[2][2]){
      System.out.println("The " + array[0][2] + " player has won");
      return true;
    } else if (array[0][0] == array[1][1] && array[0][0] == array[2][2]){
      System.out.println("The " + array[0][0] + " player has won");
      return true;
    }else  if (array[0][2] == array[1][1] && array[0][2] == array[2][0]){
      System.out.println("The " + array[0][2] + " player has won");
      return true;
    } else {
      return false;
    }
  }
    
  public static boolean checkForTie(String [][] array){
    int count = 0;
    for (int i = 0; i < array.length; i++){
      for (int j = 0; j < array[i].length; j++){
        if (array[i][j] == "O" || array[i][j] == "X"){
          count++;
        }
      }
    }
    if (count == 9){
      System.out.println("Players have tied.");
      return true;
    } else {
      return false;
    }
  }

     
  public static void main(String [] args){
    boolean winner = false;
    int count = 0;
    int pos = 0;
    String [][] gameBoard = {
      {"1", "2", "3"},
      {"4", "5", "6"},
      {"7", "8", "9"}
    };
    
    while (winner == false){
      printBoard(gameBoard);
      System.out.println("Please enter your desired position:");
      if ((count % 2) == 0) {
        pos = getPosition(gameBoard);
        switch (pos){
          case 1: 
            gameBoard[0][0] = "X";
            break;
            case 2: 
            gameBoard[0][1] = "X";
            break;
            case 3: 
            gameBoard[0][2] = "X";
            break;
            case 4: 
            gameBoard[1][0] = "X";
            break;
            case 5: 
            gameBoard[1][1] = "X";
            break;
            case 6: 
            gameBoard[1][2] = "X";
            break;
            case 7: 
            gameBoard[2][0] = "X";
            break;
            case 8: 
            gameBoard[2][1] = "X";
            break;
            case 9: 
            gameBoard[2][2] = "X";
            break;
        }
      } else {
        pos = getPosition(gameBoard);
        switch(pos){
            case 1: 
            gameBoard[0][0] = "O";
            break;
            case 2: 
            gameBoard[0][1] = "O";
            break;
            case 3: 
            gameBoard[0][2] = "O";
            break;
            case 4: 
            gameBoard[1][0] = "O";
            break;
            case 5: 
            gameBoard[1][1] = "O";
            break;
            case 6: 
            gameBoard[1][2] = "O";
            break;
            case 7: 
            gameBoard[2][0] = "O";
            break;
            case 8: 
            gameBoard[2][1] = "O";
            break;
            case 9: 
            gameBoard[2][2] = "O";
            break;
        }
      }
      count++;
      winner = checkForWin(gameBoard);
      if (winner == true){
        break;
      }
      winner = checkForTie(gameBoard);
    }
    printBoard(gameBoard);
    System.out.println("Game Over.");
  }
}

