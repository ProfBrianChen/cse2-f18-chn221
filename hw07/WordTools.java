// CSE 002 
// Hw07
// Chloe Norvell
// 29 Oct 2018
//

import java.util.Scanner; // imports scanner class

public class WordTools{
    public static void main (String [] args){
      Scanner myScanner = new Scanner (System.in);
      System.out.println("Enter your text: ");
     char menuChoice = ' ';
      String textInput = myScanner.nextLine();
      System.out.println("Your text: " + textInput);
    while (menuChoice != 'q'){
      menuChoice = printMenu(myScanner);
      
     if (menuChoice == 'c') {
      System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(textInput));
    } else if (menuChoice == 'w'){
      System.out.println("Number of words: " + getNumOfWords(textInput));
    } else if (menuChoice == 'f'){
      System.out.println("Enter word or phrase to be found: ");
      String textFound = myScanner.nextLine();
      System.out.println("\"" + textFound + "\" instances " + findText(textFound, textInput));
    } else if (menuChoice == 'r'){
      System.out.println("Edited text: " + replaceExclamation(textInput));
    } else if (menuChoice == 's'){
      System.out.println("Edited text: " + shortenSpace(textInput));
    } 
      return;
  }
    }
  public static char printMenu(Scanner myScanner){ //printMenu method
    char menuOption = ' ';
    
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    
    while (menuOption != 'c' && menuOption !='w' && menuOption != 'f' && menuOption != 'r' 
           && menuOption != 's' && menuOption != 'q'){
    System.out.println("Choose an option: ");
      menuOption = myScanner.nextLine().charAt(0);
    }
    
    return menuOption;
  }
  public static int getNumOfNonWSCharacters(String textInput){
   int numOfNonWSCharacters = 0;
    for (int i = 0; i < textInput.length(); i++){
     // char characterInput = textInput.charAt(i);
      if (textInput.charAt(i) != ' '){
        numOfNonWSCharacters++;
      }
    }
    return numOfNonWSCharacters;
  }
  public static int getNumOfWords(String textInput){
    int numOfWSCharacters = 0;
    for (int i = 0; i < textInput.length(); ++i){
      if ((textInput.charAt(i-1) == ' ') && (textInput.charAt(i) != ' ')){
        ++numOfWSCharacters;
      }
    }
    return numOfWSCharacters;
  }
  public static int findText(String textFound, String textInput){
    int wordCount = 0;
    int wordPos = 0;
    
    do {
    wordPos = textInput.indexOf(textFound);
    
    if (wordPos == -1){
      return wordCount;
    } 
      
      wordCount++; 
      textInput = textInput.substring(wordPos + 1, textInput.length());
    } while (wordPos >= 0);
    return wordCount;    
}
  
 public static String replaceExclamation(String textInput){
    return textInput.replaceAll("!", ".");
  }
  public static String shortenSpace(String textInput){
    String space = " ";
    String doubleSpace = space + space;
    
    while (textInput.indexOf(doubleSpace) != -1){
      textInput = textInput.replace(doubleSpace, space);
    }
    return textInput;
  }
}
