// ////////////
// CSE 002 
// Lab6 PatternA 
// Chloe Norvell
// 7 Nov 2018
// 
// generates random sentences

import java.util.Random;
import java.util.Scanner;

public class lab07{
  public static String firstWord() {
    Random randomGenerator = new Random();
    int intRandOne = randomGenerator.nextInt(10);
    String randomAdjective = " ";
    switch (intRandOne) {
      case 0: randomAdjective = "quick";
        break;
      case 1: randomAdjective = "big";
        break;
      case 2: randomAdjective = "small";
        break;
      case 3: randomAdjective = "smart";
        break;
      case 4: randomAdjective = "tall";
        break;
      case 5: randomAdjective = "short";
        break;
      case 6: randomAdjective = "heavy";
        break;
      case 7: randomAdjective = "wide";
        break;
      case 8: randomAdjective = "slow";
        break;
      case 9: randomAdjective = "tired";
        break;
    }
    return randomAdjective;
  }
  public static String secondWord(){
    Random randomGenerator = new Random();
    int intRandTwo = randomGenerator.nextInt(10);
    String randomSubject = " ";
    switch (intRandTwo) {
      case 0: randomSubject = "mom";
        break;
      case 1: randomSubject = "dad";
        break;
      case 2: randomSubject = "friend";
        break;
      case 3: randomSubject = "girl";
        break;
      case 4: randomSubject = "boy";
        break;
      case 5: randomSubject = "neighbor";
        break;
      case 6: randomSubject = "woman";
        break;
      case 7: randomSubject = "man";
        break;
      case 8: randomSubject = "cat";
        break;
      case 9: randomSubject = "dog";
        break;
    }
    return randomSubject;
  }
  public static String thirdWord(){
    Random randomGenerator = new Random();
    int intRandThree = randomGenerator.nextInt(10);
    String randomVerb = " ";
    switch (intRandThree) {
      case 0: randomVerb = "caught";
        break;
      case 1: randomVerb = "held";
        break;
      case 2: randomVerb = "crushed";
        break;
      case 3: randomVerb = "touched";
        break;
      case 4: randomVerb = "rolled";
        break;
      case 5: randomVerb = "stroked";
        break;
      case 6: randomVerb = "hugged";
        break;
      case 7: randomVerb = "hit";
        break;
      case 8: randomVerb = "studied";
        break;
      case 9: randomVerb = "wrote";
        break;
    }
    return randomVerb;
  }
  public static String fourthWord(){
    Random randomGenerator = new Random();
    int intRandFour = randomGenerator.nextInt(10);
    String randObject = " ";
    switch (intRandFour) {
      case 0: randObject = "kitten";
        break;
      case 1: randObject = "tree";
        break;
      case 2: randObject = "window";
        break;
      case 3: randObject = "computer";
        break;
      case 4: randObject = "class";
        break;
      case 5: randObject = "snack";
        break;
      case 6: randObject = "lunch";
        break;
      case 7: randObject = "test";
        break;
      case 8: randObject = "couch";
        break;
      case 9: randObject = "puppy";
        break;
    }
    return randObject;
  }
  public static void main (String [] args){
    Scanner myScanner = new Scanner(System.in);
    String wordOne = firstWord();
    String wordTwo = secondWord();
    String wordThree = thirdWord();
    String wordFour = fourthWord();
    System.out.println ("The " + wordOne + " " + wordTwo + " " + wordThree + " the " + wordFour + ".");
    System.out.println("Would you like another sentence? Enter 1 for yes and 2 for no.");
    int newSentence = myScanner.nextInt();
    if (newSentence == 1){
     String newWordOne = firstWord();
     String newWordTwo = secondWord();
     String newWordThree = thirdWord();
     String newWordFour = fourthWord();
    System.out.println ("The " + newWordOne + " " + newWordTwo + " " + newWordThree + " the " + newWordFour + ".");
    }
    }
    }
  
