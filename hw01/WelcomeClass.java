////////////
// CSE 002 
// HW01 Welcome Class
// Chloe Norvell
// 2 Sept 2018

public class WelcomeClass{
  public static void main(String args[]){
    // prints Welcome with border around it in first three lines
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    // prints next two lines of triangle style design
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    // prints line of design with my lehigh ID
    System.out.println("<-C--H--N--2--2--1->");
    // prints upside down version of the two lines of triangles
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
                       
  }
}