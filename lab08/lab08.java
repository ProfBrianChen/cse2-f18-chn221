// ////////////
// CSE 002 
// Lab08
// Chloe Norvell
// 14 Nov 2018
// 

import java.util.Random;

public class lab08{
  public static void main(String[] args){
    final int numElements = 100;
    int [] stringOne = new int[numElements];
    int [] stringTwo = new int[numElements];
    int [] count = new int[numElements];
    int randomNum;
    System.out.print("Array 1 holds the following integers:");
    for (int i = 0; i < numElements; i++){
      randomNum =  (int) (Math.random() * 99);
      stringOne[i] = randomNum;
      System.out.print(stringOne[i] + " ");
    }
    System.out.println();
    for(int j = 0; j < numElements; j++){
      stringTwo[stringOne[j]] += 1;
    }
    for(int k = 0; k < numElements; k++){
        System.out.println(k + " occurs " + stringTwo[k] + " times.");
    }
  }
}