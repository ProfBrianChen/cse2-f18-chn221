////////////
// CSE 002 
// Lab03 Check
// Chloe Norvell
// 13 Sept 2018
//

import java.util.Scanner; // import scanner class 

public class Check{
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in); // creating instance that will take input from STDIN
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // ask for check cost
    double checkCost = myScanner.nextDouble(); // accept user input for check cost
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");  
    // ask for tip percent
    double tipPercent = myScanner.nextDouble(); // accept user input for tip percent
    tipPercent /= 100; // convert percentage into decimal value
    
    System.out.print("Enter the number of people who went out to dinner: "); // how to split cost
    int numPeople = myScanner.nextInt(); // accept user input for number of people
    
    double totalCost; // intializing variables
    double costPerPerson;
    int dollars, // whole dollar amount of cost
    dimes, pennies; // for storing digits to the right of decimal place of cost
    
    totalCost = checkCost * (1 + tipPercent); // multiples total check by tip percent in form of 1.xx 
    // to get total cost including tip 
    costPerPerson = totalCost / numPeople; // get whole amount, dropping decimals
    dollars = (int)costPerPerson; // get dimes amount e.g., 
    // (int)(6.73*10) % 10 -> 67 % 10 -> 7
    // where the % (mod) operator returns remainder
    // after division: 583 % 100 -> 83, 27 % 5 -> 2
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson * 100) % 10;
    
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);

    
  } // end of main method
  
  
} // end of class