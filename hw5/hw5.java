// ////////////
// CSE 002 
// Hw05 
// Chloe Norvell
// 9 Oct 2018
// 
// 

import java.util.Scanner; // imports scanner class

public class hw5{
   public static void main(String[] args){
     int onePairCounter = 0, twoPairCounter = 0, threeOfAKindCounter = 0, fourOfAKindCounter = 0, fullHouseCounter = 0;
     int hold;
     
     Scanner myScanner = new Scanner (System.in); // creating instance that will take input from STDIN
     
     System.out.println("How many hands would you like to generate?");
     if (myScanner.hasNextInt()){
     int numberOfHands = myScanner.nextInt(); // creating variable for number of hands generated
     
     
     for (int i = 1; i <= numberOfHands; i++){
       int card1 = (int)(Math.random()*(52))+1;
       int card2 = (int)(Math.random()*(52))+1;
       int card3 = (int)(Math.random()*(52))+1;
       int card4 = (int)(Math.random()*(52))+1;
       int card5 = (int)(Math.random()*(52))+1;
       while(card2 == card1){ // checking if card 2 is distinct
         card2 = (int)(Math.random()*(52))+1; // generating a new card until it is distinct
       } 
       while(card3 == card1 || card3 == card2){ // checking if card 3 is distinct
         card3 = (int)(Math.random()*(52))+1; // generating a new card until it is distinct
       }
       while(card4 == card1 || card4 == card2 || card4 == card3){ // checking if card 3 is distinct
         card4 = (int)(Math.random()*(52))+1; // generating a new card until it is distinct
       }
       while(card5 == card1 || card3 == card2 || card5 == card3 || card5 == card4){ // checking if card 3 is distinct
        card5 = (int)(Math.random()*(52))+1; // generating a new card until it is distinct
       }
       // checking if face is the same, suit doesn't matter
       card1 = card1 % 13;
       card2 = card2 % 13; 
       card3 = card3 % 13;
       card4 = card4 % 13;
       card5 = card5 % 13;
       
       // sorts cards in ascending order
       for (int count = 0; count < 4; count++){ 
       if(card1 > card2 ){ 
         hold = card1;
         card1 = card2; 
         card2 = hold;
       }
       if (card2 > card3){
         hold = card2;
         card2 = card3;
         card3 = hold;
       }
       if (card3 > card4){
         hold = card3;
         card3 = card4;
         card4 = hold;
       }
       if (card4 > card5){
         hold = card4;
         card4 = card5;
         card5 = hold;
       }
         }
         
        //System.out.println(card1 + " " + card2 + " " + card3 + " " + card4 + " " + card5);
       if (card1 == card2){ // cards are sorted, if card 1 doesn't equal card 2, then card 1 has no pairs 
         if ((card1 == card3) && (card1 == card4)){ // 4 of a kind
         fourOfAKindCounter++;
         } else if (card1 == card3){ // 3 of a kind
           threeOfAKindCounter++;
           if (card4 == card5){ // if 1 = 2 = 3 and 4 = 5, it is a full house
           fullHouseCounter++;
         } else if (card3 == card4 || card4 == card5){ // 1 = 2 and (3 = 4 or 4 = 5) is two pair
         twoPairCounter++;
         } else { // one pair
           onePairCounter++;
         }
         }
         }
         
       if ((card2 == card3) && (card2 != card1)){ // card 2 and 3 are the same, but not card 1 to avoid counting a three of a kind as two pairs
         if ((card2 == card4) && (card2 == card5)){ // 4 of a kind
           fourOfAKindCounter++; 
         } else if (card2 == card4){ // three of a kind
           threeOfAKindCounter++;
         } else if (card4 == card5) { // 2 = 3 and 4 = 5 is two pair
         twoPairCounter++;
         } else { // one pair
         onePairCounter++; 
       }
       }
       if ((card3 == card4) && (card3 != card2)){ // card 3 and 4 are the same, but not card 2 to avoid counting a three of a kind as two pairs
         if (card3 == 5){ // 3 = 4 = 5 is three pair
           threeOfAKindCounter++;
           if (card1 == card2){ // 3 = 4 = 5 and 1 = 2 is full house
           fullHouseCounter++;
           }  
         } else if (card1 == card2){ // 3 doesn't = 5, but 1 = 2 and 3 = 4 so two pair
         twoPairCounter++;
         } else { // one pair
         onePairCounter++;
       }
       }
         
       if ((card4 == card5) && (card4 != card3)){ // card 3 and 4 are the same, but not card 2 to avoid counting a three of a kind as two pairs
        onePairCounter++;
       }
       
     }
     //System.out.println(onePairCounter + " " + twoPairCounter + " " + threeOfAKindCounter + " " + fourOfAKindCounter + " " + fullHouseCounter);
  
   double onePairProbability = (double)(onePairCounter) / (double)numberOfHands;
   double twoPairProbability = (double)(twoPairCounter) / (double)numberOfHands;
   double threeOfAKindProbability = (double)(threeOfAKindCounter) / (double)numberOfHands;
   double fourOfAKindProbability = (double)(fourOfAKindCounter) / (double)numberOfHands;
   double fullHouseProbability = (double)(fullHouseCounter) / (double)numberOfHands;
   
   System.out.println("The number of loops: " + numberOfHands);
   System.out.format("The probability of one pair: %.3f\n", onePairProbability); 
   System.out.format("The probability of two pairs: %.3f\n", twoPairProbability); 
   System.out.format("The probability of three of a kind: %.3f\n", threeOfAKindProbability); 
   System.out.format("The probability of four of a kind: %.3f\n", fourOfAKindProbability); 
   System.out.format("The probability of a full house: %.3f\n", fullHouseProbability); 
   
   
   
   }
   
}
}
            
       