////////////
// CSE 002 
// Hw04 CrapsIf
// Chloe Norvell
// 24 Sept 2018
//
// the program takes two dice and evalutes which numbers were rolled
// then determines the slang term for the outcome of the roll of the two dice

import java.util.Scanner; // imports scanner class

public class CrapsIf{
   public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in); // creating instance that will take input from STDIN
     System.out.println("Enter 1 to manually input the outcome of the roll. Enter 2 for the roll to be randomly generated: "); 
       // determine if user wants to enter own roll or have randomly generated roll
       int manualOrRandom = myScanner.nextInt(); 
     int dieOne;
     int dieTwo;
     String slangTerm = "hold";
  
     if (manualOrRandom == 1){ // user selected to manually input outcome
       System.out.println("Enter outcome of the roll for die 1 between 1 and 6: "); // asks for user input
        dieOne = myScanner.nextInt();
       if ((dieOne < 1) || (dieOne > 6)){ // invalid value 
         System.out.println("Invalid value. Please enter value between 1 and 6: "); // asks for new input within range of 1-6
         dieOne = myScanner.nextInt();
       }
       System.out.println("Enter outcome of the roll for die 2 between 1 and 6: "); // asks for user input
        dieTwo = myScanner.nextInt();
       if ((dieTwo < 1) || (dieTwo > 6)){ // invalid value
         System.out.println("Invalid value. Please enter value between 1 and 6: "); // asks for new input within range of 1-6
         dieTwo = myScanner.nextInt();
       }
     } else {
        dieOne = (int)(Math.random()*6)+1; // generates numbers between 1 and 6
        dieTwo = (int)(Math.random()*6)+1; // generates numbers between 1 and 6
       System.out.println("Die 1 rolled a " + dieOne + ". Die 2 rolled a " + dieTwo + ".");
     }
     if (dieOne == 1){ // cases where die 1 is a 1
       if (dieTwo == 1) {
         slangTerm = "Snake Eyes";
       } else if (dieTwo == 2){
         slangTerm = "Ace Deuce";
       } else if (dieTwo == 3){
         slangTerm = "Easy Four";
       } else if (dieTwo == 4){
         slangTerm = "Fever Five";
       } else if (dieTwo == 5){
         slangTerm = "Easy Six";
       } else if (dieTwo == 6){
         slangTerm = "Seven Out";
       }
     } else if (dieOne == 2){ // cases where die 1 is a 2
       if (dieTwo == 1){
         slangTerm = "Ace Deuce";
       } else if (dieTwo == 2) {
         slangTerm = "Hard Four";
       } else if (dieTwo == 3){
         slangTerm = "Fever Five";
       } else if (dieTwo == 4){
         slangTerm = "Easy Six";
       } else if (dieTwo == 5){
         slangTerm = "Seven Out";
       } else if (dieTwo == 6){
         slangTerm = "Easy Eight";
       } 
     } else if (dieOne == 3){ // cases where die 1 is a 3
       if (dieTwo == 1){
         slangTerm = "Easy Four";
       } else if (dieTwo == 2){
         slangTerm = "Fever Five";
       } else if (dieTwo == 3){
         slangTerm = "Hard Six";
       } else if (dieTwo == 4){
         slangTerm = "Seven out";
       } else if (dieTwo == 5){
         slangTerm = "Easy Eight";
       } else if (dieTwo == 6){
         slangTerm = "Nine";
       }
   } else if (dieOne == 4){ // cases where die 1 is a 4
       if (dieTwo == 1){
         slangTerm = "Fever Five";
       } else if (dieTwo == 2){
         slangTerm = "Easy Six";
       } else if (dieTwo == 3){
         slangTerm = "Seven Out";
       } else if (dieTwo == 4){
         slangTerm = "Hard Eight";
       } else if (dieTwo == 5){
         slangTerm = "Nine";
       } else if (dieTwo == 6){
         slangTerm = "Easy Ten";
       }
     } else if (dieOne == 5){ // cases where die 1 is a 5
       if (dieTwo == 1){
         slangTerm = "Easy Six";
       } else if (dieTwo == 2){
         slangTerm = "Seven Out";
       } else if (dieTwo == 3){
         slangTerm = "Easy Eight";
       } else if (dieTwo == 4){
         slangTerm = "Nine";
       } else if (dieTwo == 5){
         slangTerm = "Hard Ten";
       } else if (dieTwo == 6){
         slangTerm = "Yo-leven";
       }
     } else if (dieOne == 6){ // cases where die 1 is a 6
       if (dieTwo == 1){
         slangTerm = "Seven out";
       } else if (dieTwo == 2){
         slangTerm = "Easy Eight";
       } else if (dieTwo == 3){
         slangTerm = "Nine";
       } else if (dieTwo == 4){
         slangTerm = "Easy Ten";
       } else if (dieTwo == 5){
         slangTerm = "Yo-leven";
       } else if (dieTwo == 6){
         slangTerm = "Boxcars";
       }
     }
     System.out.println("You rolled a " + slangTerm); // prints slang term for roll
   }
}
       
     
   
