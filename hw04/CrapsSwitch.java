////////////
// CSE 002 
// Hw04 CrapsIf
// Chloe Norvell
// 24 Sept 2018
//
// the program takes two dice and evalutes which numbers were rolled
// then determines the slang term for the outcome of the roll of the two dice

import java.util.Scanner; // imports scanner class

public class CrapsSwitch{
   public static void main(String[] args){
     Scanner myScanner = new Scanner (System.in); // creating instance that will take input from STDIN
     System.out.println("Enter 1 to manually input the outcome of the roll. Enter 2 for the roll to be randomly generated: "); 
     // determine if user wants to enter own roll or have randomly generated roll
     int manualOrRandom = myScanner.nextInt(); 
     int dieOne = 0;
     int dieTwo = 0;
     String slangTerm = "hold";
     
     switch(manualOrRandom) {
       case 1:
         System.out.println("Enter outcome of the roll for die 1 between 1 and 6: "); // asks for user input
         dieOne = myScanner.nextInt(); 
         System.out.println("Enter outcome of the roll for die 2 between 1 and 6: "); // asks for user input
         dieTwo = myScanner.nextInt();
         break;
       case 2: 
         dieOne = (int)(Math.random()*6)+1; // generates numbers between 1 and 6
         dieTwo = (int)(Math.random()*6)+1; // generates numbers between 1 and 6
         System.out.println("Die 1 rolled a " + dieOne + ". Die 2 rolled a " + dieTwo + ".");
         break;
       default:
         System.out.println("Invalid value. Please enter 1 or 2: "); // case where invalid answer inputted
         manualOrRandom = myScanner.nextInt(); 
         break; 
     }
     
     switch(dieOne){
       case 1: 
         switch(dieTwo){
           case 1:
             slangTerm = "Snake Eyes";
             break;
           case 2: 
             slangTerm = "Ace Deuce";
             break;
           case 3:
              slangTerm = "Easy Four";
             break;
           case 4:
             slangTerm = "Fever Five";
             break;
           case 5: 
             slangTerm = "Easy Six";
             break;
           case 6: 
             slangTerm = "Seven Out";
             break;
         }
         break;
       case 2:
         switch(dieTwo){
           case 1:
             slangTerm = "Ace Deuce";
           case 2:
             slangTerm = "Hard Four";
             break;
           case 3:
             slangTerm = "Fever Five";
             break;
           case 4:
             slangTerm = "Easy Six";
             break;
           case 5:
             slangTerm = "Seven Out";
             break;
           case 6:
             slangTerm = "Easy Eight";
             break;
         }
         break;
       case 3:
         switch(dieTwo){
           case 1:
             slangTerm = "Easy Four";
           case 2:
             slangTerm = "Fever Five";
             break;
           case 3:
             slangTerm = "Fever Five";
             break;
           case 4:
             slangTerm = "Seven Out";
             break;
           case 5:
             slangTerm = "Easy Eight";
             break;
           case 6:
             slangTerm = "Nine";
             break;
         }
         break;
       case 4:
         switch(dieTwo){
           case 1:
             slangTerm = "Fever Five";
           case 2:
             slangTerm = "Easy Six";
             break;
           case 3:
             slangTerm = "Seven out";
             break;
           case 4:
             slangTerm = "Hard Eight";
             break;
           case 5:
             slangTerm = "Nine";
             break;
           case 6:
             slangTerm = "Easy Ten";
             break;
         }
         break;
       case 5:
         switch(dieTwo){
           case 1:
             slangTerm = "Easy Six";
           case 2:
             slangTerm = "Seven Out";
             break;
           case 3:
             slangTerm = "Easy Eight";
             break;
           case 4:
             slangTerm = "Nine";
             break;
           case 5:
             slangTerm = "Hard Ten";
             break;
           case 6:
             slangTerm = "Yo-leven";
             break;
         }
         break;
       case 6:
         switch(dieTwo){
           case 1:
             slangTerm = "Seven Out";
           case 2:
             slangTerm = "Easy Eight";
             break;
           case 3:
             slangTerm = "Nine";
             break;
           case 4:
             slangTerm = "Easy Ten";
             break;
           case 5:
             slangTerm = "Yo-leven";
             break;
           case 6:
             slangTerm = "Boxcars";
             break;
         }
         break;
     }
     System.out.println("You rolled a: " + slangTerm);
     } 
   }

