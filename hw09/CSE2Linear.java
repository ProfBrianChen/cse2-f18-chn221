// ////////////
// CSE 002 
// HW09
// Chloe Norvell
// 27 Nov 2018
// 

import java.util.Scanner;
import java.util.Arrays;
public class CSE2Linear{
  public static int binarySearch(int numbers [], int numbersSize, int key) {
      int mid;
      int low;
      int high;
    int count = 0;
  
      
      low = 0;
      high = numbersSize - 1;

      while (high >= low) {
        count++;
         mid = (high + low) / 2;
         if (numbers[mid] < key) {
            low = mid + 1;
         } 
         else if (numbers[mid] > key) {
            high = mid - 1;
         } 
         else {
           System.out.println(key + " was found in " + count + "iterations.");
            break;
         }
      }

     System.out.println(key + " was not found in " + count + "iterations.");
   }
public static int linearSearch(int numbers[], int numbersSize, int key) {      
      for (int i = 0; i < numbersSize; ++i) {
         if (numbers[i] == key) {
           System.out.println(key + "was found in " + (i+1) + "iterations.");
           break;
         } else {
           System.out.println(key + "was not found in " + (i+1) + "iterations.");

         }
      }
   }
 public static void main(String [] args){
   Scanner myScanner = new Scanner(System.in); 
   int numElements = 14;
   int [] gradeInputs = new int[numElements];
   String invalidInput;
   int indexSearch;
   int count = 0;
   System.out.println("Enter 15 ascending ints for final grades in CSE2:");
     for (int i = 0; i < 14; i++){
       int userInput = myScanner.nextInt();
       if (i > 0){
         while (userInput < gradeInputs[i-1]){
           System.out.println("Invalid input. Please enter integers in ascending order:");
           userInput = myScanner.nextInt();
         }
       }
        if (userInput < 1 || userInput > 100){
         System.out.println("Invalid input. Please enter an integer 1-100:");
         userInput = myScanner.nextInt();
       }
       if (!myScanner.hasNextInt()){ // enters loop if input isn't an integer
         invalidInput = myScanner.next(); // removes input from 'conveyer belt'
         System.out.println("Invalid input. Please enter an integer:");
         userInput = myScanner.nextInt();
      }
       gradeInputs [i] = userInput;
     }
   
   System.out.println(Arrays.toString(gradeInputs));
   
   System.out.println("Enter a value to search for: ");
 if(myScanner.hasNextInt()){
   int searchGrade = myScanner.nextInt();
   indexSearch = binarySearch(gradeInputs, numElements, searchGrade);
   Scramble(gradeInputs);
   System.out.println("Scrambled:");
   System.out.println(Arrays.toString(gradeInputs));
   System.out.println("Enter a value to search for: ");
   if(myScanner.hasNextInt()){
   searchGrade = myScanner.nextInt();
   linearSearch(gradeInputs, numElements, linearSearchGrade);
   } else {
      System.out.println("Invalid input. Please enter an integer:");
      linearSearchGrade = myScanner.nextInt();
   }
   } else {
   System.out.println("Invalid input. Please enter an integer:");
   searchGrade = myScanner.nextInt();
 }
   
 }
   
   
  // if (indexSearch == -1){
    // System.out.println("Grade not found");
  // } else {
    // count += 1;
  // }
   //System.out.println(count);
 } 

