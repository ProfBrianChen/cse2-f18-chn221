////////////
// CSE 002-310 Arithmetic
// Chloe Norvell
//

public class Arithmetic {
  public static void main(String[] args) {
  //intializing item and item price variables
    int numPants = 3; //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants
   
    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt
   
    int numBelts = 1; //Number of belts
    double beltPrice = 33.99; //cost per belt
    
    double paSalesTax = 0.06; //the tax rate
    
    // initializing total cost of each item
    double totalCostOfPants = numPants * pantsPrice;   //total cost of pants
    double totalCostOfShirts = numShirts * shirtPrice; //total cost of shirts
    double totalCostOfBelts = numBelts * beltPrice; // total cost of belts
    
    
    // print cost of each item
    System.out.println("The total cost of pants is $" + totalCostOfPants);
    System.out.println("The total cost of shirts is $" + totalCostOfShirts); 
    System.out.println("The total cost of belts is $" + totalCostOfBelts);
    
    //initialize sales tax on each item
    double pantsSalesTax = totalCostOfPants * paSalesTax;
    double shirtsSalesTax = totalCostOfShirts * paSalesTax;
    double beltsSalesTax = totalCostOfBelts * paSalesTax;
    
    // truncate values to two decimals
    pantsSalesTax = ((int)(pantsSalesTax*100))/100.0;
    shirtsSalesTax = ((int)(shirtsSalesTax*100))/100.0;
    beltsSalesTax = ((int)(beltsSalesTax*100))/100.0;
    
    
    // print sales tax on each item
    System.out.println("The sales tax on pants is $" + pantsSalesTax);
    System.out.println("The sales tax on shirts is $" + shirtsSalesTax);
    System.out.println("The sales tax on belts is $" + beltsSalesTax);
    
    
    // evaluate and print totals
    double totalCostWithoutTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    double totalSalesTax = pantsSalesTax + shirtsSalesTax + beltsSalesTax;
    double totalCostWithTax = totalCostWithoutTax + totalSalesTax;
    System.out.println("The total cost of all items before tax is $" + totalCostWithoutTax);
    System.out.println("The total sales tax on all items is $" + totalSalesTax);
    System.out.println("The total cost of all items after tax is $" + totalCostWithTax);

  }
}