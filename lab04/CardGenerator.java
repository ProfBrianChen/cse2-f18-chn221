////////////
// CSE 002 
// Lab 04 Card Generator
// Chloe Norvell
// 18 Sept 2018
//
// this programs randomly generates a number from 1-52, where the numbers represent a card from a deck

public class CardGenerator{
  public static void main(String[] args){
    String cardSuit1 = "Diamonds";
    String cardSuit2 = "Clubs";
    String cardSuit3 = "Hearts";
    String cardSuit4 = "Spades";
    String cardSuit = "hold";
   
    int randomNumber = (int)(Math.random()*52)+1; // generates numbers between 1 and 52
    
    if ((randomNumber >= 1) && (randomNumber <=13)){ // if card number is between 1 and 13 it is a diamond card 
      cardSuit = cardSuit1;
      int cardStatus = randomNumber % 13; // number modulo 13 
      
     switch (cardStatus) // handles special cases
     { 
       case 1: 
         System.out.println("You picked the Ace of " + cardSuit); // remainder was 1 so card is an ace
         break;
       case 11:
         System.out.println("You picked the Jack of " + cardSuit); // remainder was 11 so card is a jack
         break;
       case 12:
         System.out.println("You picked the Queen of " + cardSuit); // remainder was 12 so card is a queen
         break;
       case 0:
         System.out.println("You picked the King of " + cardSuit); // remainder was 13 so card is a king
         break;
       default:
         System.out.println("You picked the " + cardStatus + " of " + cardSuit); 
         // not a special case so just prints randomly generated number and corresponding suit   
     }
    }
    
    if ((randomNumber >= 14) && (randomNumber <=26)){ // if card number is between 14 and 26 it is a clubs card 
      cardSuit = cardSuit2;
      int cardStatus = randomNumber % 13; // number modulo 13 
      
     switch (cardStatus) // handles special cases
     { 
       case 1: 
         System.out.println("You picked the Ace of " + cardSuit); // remainder was 1 so card is an ace
         break;
       case 11:
         System.out.println("You picked the Jack of " + cardSuit); // remainder was 11 so card is a jack
         break;
       case 12:
         System.out.println("You picked the Queen of " + cardSuit); // remainder was 12 so card is a queen
         break;
       case 0:
         System.out.println("You picked the King of " + cardSuit); // remainder was 13 so card is a king
         break;
       default:
         System.out.println("You picked the " + cardStatus + " of " + cardSuit); 
         // not a special case so just prints randomly generated number and corresponding suit   
     }
    }
    
    if ((randomNumber >= 27) && (randomNumber <= 39)){ // if card number is between 27 and 39 it is a hearts card 
      cardSuit = cardSuit3;
      int cardStatus = randomNumber % 13; // number modulo 13 
      
     switch (cardStatus) // handles special cases
     { 
       case 1: 
         System.out.println("You picked the Ace of " + cardSuit); // remainder was 1 so card is an ace
         break;
       case 11:
         System.out.println("You picked the Jack of " + cardSuit); // remainder was 11 so card is a jack
         break;
       case 12:
         System.out.println("You picked the Queen of " + cardSuit); // remainder was 12 so card is a queen
         break;
       case 0:
         System.out.println("You picked the King of " + cardSuit); // remainder was 13 so card is a king
         break;
       default:
         System.out.println("You picked the " + cardStatus + " of " + cardSuit); 
         // not a special case so just prints randomly generated number and corresponding suit   
     }
    }
    
    if ((randomNumber >= 40) && (randomNumber <= 52)){ // if card number is between 40 and 52 it is a hearts card 
      cardSuit = cardSuit4;
      int cardStatus = randomNumber % 13; // number modulo 13 
      
     switch (cardStatus) // handles special cases
     { 
       case 1: 
         System.out.println("You picked the Ace of " + cardSuit); // remainder was 1 so card is an ace
         break;
       case 11:
         System.out.println("You picked the Jack of " + cardSuit); // remainder was 11 so card is a jack
         break;
       case 12:
         System.out.println("You picked the Queen of " + cardSuit); // remainder was 12 so card is a queen
         break;
       case 0:
         System.out.println("You picked the King of " + cardSuit); // remainder was 13 so card is a king
         break;
       default:
         System.out.println("You picked the " + cardStatus + " of " + cardSuit); 
         // not a special case so just prints randomly generated number and corresponding suit   
     }
    }
    
   
  }
}
