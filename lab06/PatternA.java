// ////////////
// CSE 002 
// Lab6 PatternA 
// Chloe Norvell
// 11 Oct 2018
// 
// 

import java.util.Scanner; // imports scanner class

public class PatternA{
   public static void main(String[] args){
     int innerLoop, i, startNum;
     String invalidInput;
     Scanner myScanner = new Scanner (System.in); // creating instance that will take input from STDIN
    
     // int pyramidLength = myScanner.nextInt();
     System.out.println("Enter a number between 1 and 10: ");

     while (!myScanner.hasNextInt()){ // enters loop if input isn't an integer
       invalidInput = myScanner.next(); // removes input from 'conveyer belt'
     System.out.println("Invalid input. Please enter an integer 1-10:");
      }
   
    int pyramidLength = myScanner.nextInt(); // stores valid input
     
     if (pyramidLength < 1 || pyramidLength > 10){ // rejects inputs outside accepted range
       System.out.println("Invalid input. Please enter an integer 1-10:");
     }
     
     for (i = 0; i < pyramidLength; i++){ // increments from 0 to inputted length
       startNum = 1;
      for (innerLoop = 0; innerLoop <= i; innerLoop++){
         System.out.print(startNum + " ");
        startNum++;
      }
       System.out.println("");
     }
    
    }
   }
