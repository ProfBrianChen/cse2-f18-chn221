// ////////////
// CSE 002 
// Lab6 PatternB
// Chloe Norvell
// 11 Oct 2018
// 
// 

import java.util.Scanner; // imports scanner class

public class PatternB{
   public static void main(String[] args){
     int innerLoop, i;
     String invalidInput;
     Scanner myScanner = new Scanner (System.in); // creating instance that will take input from STDIN
    
     // int pyramidLength = myScanner.nextInt();
     System.out.println("Enter a number between 1 and 10: ");

     while (!myScanner.hasNextInt()){ // enters loop if input isn't an integer
       invalidInput = myScanner.next(); // removes input from 'conveyer belt'
         System.out.println("Invalid input. Please enter an integer 1-10:");
     }
   
    int pyramidLength = myScanner.nextInt(); // stores valid input
     
     if (pyramidLength < 1 || pyramidLength > 10){ // rejects inputs outside accepted range
       System.out.println("Invalid input. Please enter an integer 1-10:");
     }
     
     for (i = pyramidLength; i >= 1; --i){ // increments backwards so pyramid decreases
      for (innerLoop = 1; innerLoop <= i; ++innerLoop){ // adds increasing integers to line
       System.out.print(innerLoop + " ");
      }
       System.out.println(""); // adds new line after loop 
     }
    
    }
   }
