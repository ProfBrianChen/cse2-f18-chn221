// ////////////
// CSE 002 
// Lab6 PatternD
// Chloe Norvell
// 11 Oct 2018
// 
// 

import java.util.Scanner; // imports scanner class

public class PatternD{
   public static void main(String[] args){
     int innerLoop, i, pyramidLength;
     String invalidInput;
     
     Scanner myScanner = new Scanner (System.in); // creating instance that will take input from STDIN
    
     System.out.println("Enter a number between 1 and 10: ");
   
 while (!myScanner.hasNextInt()){ 
     invalidInput = myScanner.next();
     System.out.println("Invalid input. Please enter an integer 1-10:");
     
   }

   pyramidLength = myScanner.nextInt(); // stores valid input
    
     if (pyramidLength < 1 || pyramidLength > 10){ // rejects inputs outside accepted range
       System.out.println("Invalid input. Please enter an integer 1-10:");
     }
     
     for (i = pyramidLength; i >= 1; --i){ // increments backwards 
      for (innerLoop = i; innerLoop > 0; --innerLoop){ // adds decreasing integers to line
       System.out.print(innerLoop + " ");
      }
       System.out.println(""); // adds new line after loop
     }
    
    }
   }
