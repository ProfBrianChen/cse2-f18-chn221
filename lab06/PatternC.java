// ////////////
// CSE 002 
// Lab6 PatternC 
// Chloe Norvell
// 11 Oct 2018
// 
// 

import java.util.Scanner; // imports scanner class

public class PatternC{
   public static void main(String[] args){
     int innerLoop, i;
     String invalidInput;
     Scanner myScanner = new Scanner (System.in); // creating instance that will take input from STDIN
    
     // int pyramidLength = myScanner.nextInt();
     System.out.println("Enter a number between 1 and 10: ");

     while (!myScanner.hasNextInt()){ // enters loop if input isn't an integer
      invalidInput = myScanner.next(); // removes input from 'conveyer belt'
          System.out.println("Invalid input. Please enter an integer 1-10:");
     }
   
    int pyramidLength = myScanner.nextInt(); // stores valid input
     
     if (pyramidLength < 1 || pyramidLength > 10){ // rejects inputs outside accepted range
       System.out.println("Invalid input. Please enter an integer 1-10:");
     }
     
     for (i = 0; i < pyramidLength+1; i++){ // loops from 0-pyramid length + 1
       for (int j = pyramidLength+1; j > 0; j--){  // increments backwards from pyramidlength + 1 to 0
         if (i < j){ 
           System.out.print(" "); // adds enough spaces to right orient pyramid
         }
         else{
           System.out.print(j); // prints the value of j 
         }
       }
       System.out.println(""); // adds new line between loops
     }
    
    }
   }


