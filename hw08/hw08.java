// ////////////
// CSE 002 
// HW08
// Chloe Norvell
// 7 Nov 2018
// 

import java.util.Scanner;
import java.util.Random;
 public class hw08{ 

public static void printArray(String [] list){
  System.out.println("System Output:");
  for(int i = 0; i < list.length; i++){
    System.out.print(list[i] + " "); 
  }
  System.out.println();
}
public static void shuffle(String [] list){
  String hold;
  for(int i = 0; i < 66; i++){
    int randCard = (int) (Math.random() * 51) + 1;
    hold = list[0];
     list[randCard] = hold;
  }
}
public static String [] getHand(String [] list, int index, int numCards){
  String [] getHand = { "", "", "", "", ""};
  for (int j = 0; j < numCards; j++){
    getHand[j] = list[index-j];
  }
  return getHand;
}

public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  } 
}
