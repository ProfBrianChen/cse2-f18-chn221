////////////
// CSE 002-310 Cyclometer
// Chloe Norvell
// Given time and rotation count of two bike trips, program gives time in minutes of trip
// number of counts for each trip, distance in miles of each trip, and combined distance of the two trips

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
      // input data
      int secsTrip1 = 480;  // time in seconds of the first trip
      int secsTrip2 = 3220;  // time in seconds of second trip
      int countsTrip1 = 1561;  // number of wheel rotations in trip one
      int countsTrip2 = 9037; // rotation count in trip two
      
      // variables and output data
      double wheelDiameter = 27.0,  // diameter of bike wheel
      PI = 3.14159, // value of pi
      feetPerMile = 5280,  // feet in a mile
      inchesPerFoot = 12,   // inches in a foot
      secondsPerMinute = 60;  // seconds in a minute
      double distanceTrip1, distanceTrip2,totalDistance;  // variables for individual and combined trip distances
    
      System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
      // output of trip one's time in minutes and rotation count
      System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
      // output of trip two's time in minutes and rotation count
      
      distanceTrip1 = countsTrip1 * wheelDiameter * PI; // distance in inches
    	//(for each count, a rotation of the wheel travels the diameter in inches times PI)
      distanceTrip1 /= inchesPerFoot * feetPerMile; // distance in miles
      distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; 
      totalDistance = distanceTrip1 + distanceTrip2;
      
      //Print output data. 
      System.out.println("Trip 1 was "+distanceTrip1+" miles"); // distance of trip 1 in miles
      System.out.println("Trip 2 was "+distanceTrip2+" miles"); // distance of trip 2 in miles
      System.out.println("The total distance was "+totalDistance+" miles"); // combined distance of both trips in miles

	}  //end of main method   
} //end of class
